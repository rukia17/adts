#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout << "Welcome to my List ADT client"<<endl<endl;

 cout << L1.size();
 cout << L2.size();

 L1.insert(1,1);
 L1.insert(2,2);
 L2.insert(1,1);
 L2.insert(2,2);

cout << L1.get(1,1);
cout << L1.get(2,2);
cout << L2.get(1,1);
cout << L2.get(2,2);

L1.remove(1);
L2.remove(2);

~List();


return 0;

}
